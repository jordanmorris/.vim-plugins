"http://vim-scripts.org
Plugin 'repeat.vim'
Plugin 'ctrlp.vim'
Plugin 'tComment'
Plugin 'AutoComplPop'
Plugin 'L9'
Plugin 'rails.vim'
Plugin 'The-NERD-tree'
Plugin 'trailing-whitespace'

"randoms
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-rbenv'
Plugin 'jordanmorris/woggle.vim'

"personal
Plugin 'http://bitbucket.org/jordanmorris/vim-config.git'
Plugin 'http://bitbucket.org/jordanmorris/vim-config-windows.git'
"Plugin 'http://bitbucket.org/jordanmorris/vim-config-linux.git'
